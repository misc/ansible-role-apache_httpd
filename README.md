Ansible module used to manage httpd.

This role is a complete rewrite of the role I previously used, in order to
be faster to deploy and easier to maintain.

# Philosophy

in order to be simple, some opinionated and radical choices have been made,
rendering it likely unsuitable for anyone but the original author specific use
cases.

Everything do boil down to the requirements. The first is that
the role is here for a personal server exposed on the internet. Nothing more,
nothing less, and we will see the implication and how, coupled with radical 
simplicity, it drive the design and explain the choices made.

## The base unit is a vhost

While not everybody use that pattern, the system do assume that 
everything is done in a vhost and that the vhost is dedicated to a single purpose.
It also assume that the vhost is deployed on 1 single server, no HA or anything
fancy.

For example, there is no support for having /calendar being owncloud and /mail being
Zimbra. A application is on the top level and that's it. While this seems radical and not
flexible, this greatly simplify the code. The intent of the vhost is expressed with the specific
role used (vhost_proxy, vhost_static), which in turn fix subtle issues of variables leaking, 
and prevent problem caused by combination that weren't planned (like using proxy and redirection
at the same time).

It also help on the web security front with CORS and cookies.

## Minimal support for http

The role do always redirect to https. This do simplify the design because there
is no need to share configuration between http and https vhost since the http part
is pretty much fixed. In turn, this also simplify all kind of interaction. 
For example, in the previous role, setting a redirection and forcing
TLS would use the same mechanism, resulting in surprising results.

This do mean that internal LAN servers are not supported. I might add something
if the need arise, but as I do have IP v6, that shouldn't be a problem. 

## No support for anything but mod_md

Since the role mandate HTTPS, we need certificates. While previous role did
permit to use freeipa, certbot and a custom certificate (self signed or not), this 
one support only mod_md using http01 challenge. Again, this mean the server need 
to be on the internet, but this greatly simplify the setup, since mod_md take 
care of everything.

No need to set cron, or a custom DNS stack, or specific bypass or the configuration
for the challenge, nor bootstrapping problem.

## No support for most platforms

The requirement of using mod_md limit the platform to a modern Linux distribution.
This role was written for a server running Fedora 32. 
I might add Centos 8 support if that work out of the box and have a need, but the role will just try 
to stay current on what is present on Fedora.
 
## Use default upstream configuration as much as possible

While there is value into using specific ciphers for specific cases, 
advanced crypto attacks are not part of my threat model, but losing time
dealing with crypto issues is. So I do not plan to add complexity 
to handle changing the ciphers or any configuration.

I do assume Fedora default would be good enough for my purpose.

# Examples

## Declare a static website

```
- hosts: web
  vars:
    mail_domain: example.org
  roles:
  - name: apache_httpd/vhost_static
    vhost: static.example.org
    document_root: /var/www/static.example.org
```

The handling of the DocumentRoot directory is outside
of the scope of the role.

## Declare a redirected website

```
- hosts: web
  vars:
    mail_domain: example.org
  roles:
  - name: apache_httpd/vhost_redirect
    vhost: redirect.example.org
    destination: https://static.example.org/redirected
```

## Declare a proxy

```
- hosts: web
  vars:
    mail_domain: example.org
  roles:
  - name: apache_httpd/vhost_proxy
    vhost: proxy.example.org
    server: soft.int.example.org
    remote_port: 8080
```

## Declare a fastCGI/etc server

```
- hosts: web
  vars:
    mail_domain: example.org
  roles:
  - name: apache_httpd/vhost_proxy
    vhost: django.example.org
    path: /var/run/web_software.sock
```

# Missing features from previous roles
## Tor support

While I do support tor, it is unlikely to be compatible with
with "use https by default". I may write a custom role for that later, if 
the need arise.

## Password protection

There is no plan to add password protection. Since the role is for
personal server, I can restrict without having to share a password or anything. 

## Server aliases

Since redirects are enough for that, support for aliases is unlikely to be added.

## Custom security/performance tuning

Since neither complex optimization nor complete security policy are what I plan to 
write, this is not going to be added. There is likely no need for fine performance 
tuning, and security settings (CSP, etc) should be pushed in the applications.

## Extensibility with drop-in config files

It is not planned to support that. While extensibility is sometime a feature, it
is also against the main idea for that role. For example, each vhost is in 1 single
file so it is easier to remove, and having a dropin configuration system would go against that.

## Public_html support

While public_html is a convenient feature for a personal server, I do not have a need for
now to use it. It might be added later if I find a elegant solution (likely a new
vhost type, or a option on static vhost).
